from bs4 import BeautifulSoup, Tag
import os
import shutil
import glob
import re

target_dir = "/home/sbernard/git/ldt/documentation/LDT"
destination_dir = "/home/sbernard/git/ldt/documentation"
image_dir = "/home/sbernard/git/ldt/documentation/images"
before = "/home/sbernard/git/ldt/documentation/before"
after = "/home/sbernard/git/ldt/documentation/after"
rootURL = "/ldt/documentation/"
imageURL = rootURL + "images/"

# utility 
def replaceBeginBy(original_str, to_replace, replacement):
    if original_str.startswith(to_replace):
        return replacement + original_str[len(to_replace):]
    return original_str

def find_file_case_insensitive(directory, filename):
    filename_lower = filename.lower()
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.lower() == filename_lower:
                return file
            if file.lower() == filename_lower.replace(".jpg",".png"):
                return file
    return None

# catch files
html_files = glob.glob(os.path.join(target_dir, '**/*.html'), recursive=True)

print(html_files)

# for each files
for html_file in html_files:
    # create define new path / names
    relpath = os.path.relpath(html_file, target_dir)
    outname = os.path.join(destination_dir, relpath)
    
    # create folders
    os.makedirs(os.path.dirname(outname), exist_ok=True)

    # add header + footer
    with open(outname, 'w') as output:
        with open(before, 'r') as before_file:
            output.write(before_file.read())
        
        with open(html_file, 'r') as current_file:
            output.write(current_file.read())
        
        with open(after, 'r') as after_file:
            output.write(after_file.read())
    
        print(f"Converted {html_file} to {outname}")

    # Clean file
    with open(outname, 'r') as file:
        html = file.read()

    soup = BeautifulSoup(html, 'html.parser')
    # lower case id
    for element in soup.find_all(id=True):
        element['id'] = '_'.join(word.lower() for word in element['id'].split('_'))

    # fix images 
    images_with_src = soup.find_all('img', src=True)
    for img in images_with_src:
        url = img['src'] 
        file = find_file_case_insensitive( image_dir,url)
        if file:
            img['src'] = imageURL + file
        else:
            print("=====> CAN NOT FIND FILE : " + url)
            
    # fix code balise
    paragraphs = soup.find_all('p')
    for p in paragraphs:
        # if p doesn't contain text directly
        res = p.find(string=True, recursive=False)
        if res is None or res.string.isspace():
            code_children = p.find_all('code')
            if len(code_children) == 1 and len(p.findChildren()) == 1:
                # replace code by pre when needed
                code_child = code_children[0]
                pre_element = soup.new_tag('pre')
                if code_child.string :
                    pre_element.string = code_child.string
                    code_child.replace_with(pre_element)
                else:
                    code_child.decompose()
            elif len(code_children) > 1:
                # merge multi code in pre
                children_not_br = [child for child in p.children if isinstance(child, Tag) and child.name != 'br']
                if len(code_children) == len(children_not_br):
                    pre_element = soup.new_tag('pre')
                    codes_string = []
                    for code in code_children:
                        codes_string.append(code.string)
                    p.clear()
                    pre_element.string = "\n".join(codes_string)
                    p.append(pre_element)

    # Remove figure Caption
    for fig in soup.find_all('figcaption'):
        if fig['aria-hidden']:
            fig.decompose()
    
    #remove wikitile
    for element in soup.find_all(title=True):
        if  element['title'].startswith("wikilink"):
            del element['title']
    
    # fix href
    for element in soup.find_all(href=True):
        url=element['href']
        anchor = None
        href_parts = url.split('#')
        if len(href_parts) > 1:
            anchor = href_parts[-1]
            url = href_parts[0]
        
        # fix url 
        if url:
            url = replaceBeginBy(url,"wiki.eclipse.org/LDT/", rootURL)
            url = replaceBeginBy(url,"http://wiki.eclipse.org/LDT/", rootURL)
            url = replaceBeginBy(url,"https://wiki.eclipse.org/LDT/", rootURL)
            if url.startswith("http://git.eclipse.org/c/ldt/org.eclipse.ldt.git/tree/"):
                url = url.replace(".koneki.", ".")
            url = replaceBeginBy(url,"http://git.eclipse.org/c/ldt/org.eclipse.ldt.git/tree/", "https://gitlab.eclipse.org/eclipse/ldt/ldt/-/blob/1.4.2RC2/")
            if url.startswith("git://git.eclipse.org/gitroot/ldt/org.eclipse.ldt.git"):
                url = url.replace("git://git.eclipse.org/gitroot/ldt/org.eclipse.ldt.git", "https://gitlab.eclipse.org/eclipse/ldt/ldt/")
            url = replaceBeginBy(url,"http://git.eclipse.org/c/ldt/org.eclipse.ldt.git/tree/", "https://gitlab.eclipse.org/eclipse/ldt/ldt/-/blob/1.4.2RC2/")
            url = replaceBeginBy(url,"LDT/", rootURL)
            if url.startswith(rootURL) and not bool(re.search(r"\.[a-zA-Z]+$", url)):# if there is no extension add it
                url = url+".html"
            if url.startswith(rootURL+"Developer_Area/User_Guides/User_Guide_"):
                url = rootURL + "User_Area/User_Guides/User_Guide_1.4.html"
            if url.startswith(rootURL+"User_Area/User_Guides/User_Guide_"):
                url = rootURL + "User_Area/User_Guides/User_Guide_1.4.html"

        if element.string and element.string.startswith("http://git.eclipse.org/c/ldt/org.eclipse.ldt.git/tree/"):
            element.string = replaceBeginBy(element.string,"http://git.eclipse.org/c/ldt/org.eclipse.ldt.git/tree/", "https://gitlab.eclipse.org/eclipse/ldt/ldt/-/blob/1.4.2RC2/")
            element.string = element.string.replace(".koneki.", ".")

        if element.string and element.string.startswith("git://git.eclipse.org/gitroot/ldt/org.eclipse.ldt.git"):
                element.string = element.string.replace("git://git.eclipse.org/gitroot/ldt/org.eclipse.ldt.git", "git@gitlab.eclipse.org:eclipse/ldt/ldt.git")

        # lower case anchor
        if anchor:
            anchor = anchor.lower()  # Lowercase the anchor part
            element['href'] = url+'#'+anchor
        else:
            element['href'] = url

    with open(outname, 'w') as file:
        file.write(str(soup))

