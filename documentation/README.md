Initially documentation was written at https://wiki.eclipse.org/LDT.  
But eclipse foundation decide to close this wiki : https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/wikis/Wiki-shutdown-plan

So we export the wikipage. Original Export was available in : export_wiki.xml  
Then we convert it to html with : `docker run -v $PWD:/app thawn/mediawiki-to-gfm --filename=export_wiki.xml --format=html5`   
(see : https://github.com/outofcontrol/mediawiki-to-gfm/issues/29)  
Result of this conversion is available in LDT directory.  
Then we clean this export with `clean_python.py` script.

Result of this is available at https://gitlab.eclipse.org/eclipse/ldt/ldt-website/-/tree/master/documentation?ref_type=heads   
Images from are also available over there. 
 
