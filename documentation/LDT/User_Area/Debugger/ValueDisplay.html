<p>This document introduces the concept of custom inspectors which allows you to plug your own logic to decode children properties debugging.</p>
<p>This document is a tutorial, it just introduces concepts and best practices. For details about used functions, see the <a href="http://git.eclipse.org/c/koneki/org.eclipse.koneki.ldt.git/tree/libraries/luadbgpclient/debugger/introspection.lua?id=1.1_RC1"><code>debugger.introspection</code></a> API documentation.</p>
<p>There are two main uses cases:</p>
<ul>
<li>Enumerate properties of userdata object (which debugger cannot inspect at all out of the box)</li>
<li>Customize debugging of tables to hide internal fields and reduce noise</li>
</ul>
<p>The provided API tries to be as simple as possible by hiding most debugger &lt;=&gt; IDE communication logic such as recursion limit, pagination, … However it is not yet considerated as final and may change without notice.</p>
<h1 id="inspector_modules">Inspector modules</h1>
<p>The inspectors are module distinct from the debugger and must be registered in order to be active. It can be done before or after starting debugging.</p>
<p>For simplicity, inspector modules should register themselves into debugger core and do not return anything. So attaching a module is simply a <code>require</code> or <code>dofile</code> call. This violates the <em>no global side effects</em> policy of Lua modules, but for debugging purposes, I think we can live with that :)</p>
<h1 id="probing_objects">Probing objects</h1>
<p>First, we need to know what object we want to inspect. For this, two distinct modes are available.</p>
<h2 id="metatable_matching">Metatable matching</h2>
<p>This matching mode works by associating an inspector function to a given metatable. It requires you to know in advance what metatables are expected (for example, all your <em>Car</em> objects share the same metatable).</p>
<p>For this we simply add our inspector function to <code>debugger.introspection.inspectors</code> table with the metatable as key. Your module should look like:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> introspection <span class="op">=</span> <span class="fu">require</span> <span class="st">&#39;debugger.introspection&#39;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> function my_inspector<span class="op">(</span>name<span class="op">,</span> value<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>      <span class="co">-- my inspection logic</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>    <span class="cf">end</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>inspectors<span class="op">[</span>my_metatable<span class="op">]</span> <span class="op">=</span> my_inspector</span></code></pre></div>
<h2 id="generic_probe">Generic probe</h2>
<p>Some libraries use a lot of different metatables or create them on-the-fly. In this situation, it can became tricky to list all possible metatables for a given object family.</p>
<p>To handle such cases, you can add a probe function that will be called for any value (table or userdata) when a specific metatable inspector cannot be found.</p>
<p>As you can imagine, this method is munch slower than previous one so do not abuse this feature by attaching many such probes or making expensive processing on them.</p>
<p>Generic probes are added to debugger with <code>debugger.introspection.add_probe</code> function. A typical module should look like:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> introspection <span class="op">=</span> <span class="fu">require</span> <span class="st">&#39;debugger.introspection&#39;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> function my_inspector<span class="op">(</span>name<span class="op">,</span> value<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>      <span class="cf">if</span> is_my_object<span class="op">(</span>value<span class="op">)</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>        <span class="co">-- my inspection logic</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>      <span class="cf">end</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>      <span class="cf">return</span> <span class="kw">nil</span> <span class="co">-- unknown object</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>    <span class="cf">end</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a>    </span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>add_probe<span class="op">(</span>my_inspector<span class="op">)</span></span></code></pre></div>
<h1 id="inspection_logic">Inspection logic</h1>
<p>Now we’re able to detect values to inspect, let’s generate our properties. This what inspector functions do.</p>
<p>These functions generate properties that will be sent to IDE with the <code>debugger.introspection.property</code> function. They are called with a value to inspect and must generate a debugger property for it (and possibly its children for complex data structures).</p>
<p>Like we saw in previous part, inspector functions always takes 4 parameters and their return values are defined too.</p>
<p>These arguments are:</p>
<ul>
<li><strong>name</strong> is the property name that appear on the <em>name</em> column of debugger view. This name is usually given by parent object (that is the inspector that called your inspector)</li>
<li><strong>value</strong> is the actual value to inspect</li>
<li><strong>parent</strong> is the parent property of the value (a debugger data structure, not <code>value</code>’s parent !)</li>
<li><strong>fullname</strong> this is the expression that will be evaluated by the debugger to get <code>value</code> again (either to query its children or to set it).</li>
</ul>
<h2 id="generating_properties">Generating properties</h2>
<p>The main task of the inspector is to call <code>debugger.introspection.property</code> to generate a property corresponding to <code>value</code>. This function will check if the value should be sent to IDE, depending on this, it returns:</p>
<ul>
<li>A <em>Property</em> object if the property have be sent to IDE, in this case you can inspect children properties (if any) and return that object to caller</li>
<li><code>nil</code> if value will not be sent to IDE (too deep recursion for instance). In this case, you should not try to inspect children and return a <code>nil</code> value immediately.</li>
</ul>
<p>The inspector that is used for most primitive types is simply:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> function default_inspector<span class="op">(</span>name<span class="op">,</span> value<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>      <span class="cf">return</span> introspection<span class="op">.</span>property<span class="op">(</span>name<span class="op">,</span> <span class="fu">type</span><span class="op">(</span>value<span class="op">),</span> <span class="fu">tostring</span><span class="op">(</span>value<span class="op">),</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>    <span class="cf">end</span></span></code></pre></div>
<h2 id="inspecting_children_properties">Inspecting children properties</h2>
<p>Now let’s inspect more complex types with child properties. This is done by either by calling <code>debugger.introspection.property</code> more than once if you want to fully handle children inspection or by calling <code>debugger.introspection.inspect</code> to dispatch an arbitrary value to appropriate inspector.</p>
<p>As you can imagine, the <code>parent</code> of these sub-properties is the object returned by <code>debugger.introspection.property</code>. For <code>fullname</code> (the expression used to retrieve the value in future calls), you can use <code>debugger.introspection.make_fullname</code> to generate a valid Lua expression However, this method works only if your property can be accessed by index.</p>
<p>If the value is retrieved by some other way (like calling a <code>get_my_prop</code> function) there is currently no supported way to handle it, see <em>Limitations</em> section).</p>
<p>Let’s show an example with a <em>Car</em> that have <em>x</em>, <em>y</em>, <em>z</em> and <em>speed</em> properties that we want to expose and some internal <em>id</em> that we will use only for string representation:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> function car_inspector<span class="op">(</span>name<span class="op">,</span> value<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>      <span class="kw">local</span> prop <span class="op">=</span> introspection<span class="op">.</span>property<span class="op">(</span>name<span class="op">,</span> <span class="st">&#39;Car&#39;</span><span class="op">,</span> <span class="st">&#39;Car #&#39;</span> <span class="op">..</span> value<span class="op">.</span>id<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>      <span class="co">-- do not inspect children if the parent is not generated</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a>      <span class="cf">if</span> <span class="kw">not</span> prop <span class="cf">then</span> <span class="cf">return</span> <span class="kw">nil</span> <span class="cf">end</span></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>      </span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a>      <span class="co">-- use regular introspection for x,y,z</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a>      introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;x&#39;</span><span class="op">,</span> value<span class="op">.</span>x<span class="op">,</span> prop<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;x&#39;</span><span class="op">))</span></span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a>      introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;y&#39;</span><span class="op">,</span> value<span class="op">.</span>y<span class="op">,</span> prop<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;y&#39;</span><span class="op">))</span></span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a>      introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;z&#39;</span><span class="op">,</span> value<span class="op">.</span>z<span class="op">,</span> prop<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;z&#39;</span><span class="op">))</span></span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a>      </span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a>      <span class="co">-- generate directly speed property</span></span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a>      introspection<span class="op">.</span>property<span class="op">(</span><span class="st">&#39;speed&#39;</span><span class="op">,</span> <span class="st">&#39;Speed&#39;</span><span class="op">,</span> <span class="fu">tostring</span><span class="op">(</span>value<span class="op">.</span>speed<span class="op">),</span> prop<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;speed&#39;</span><span class="op">))</span></span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a>      </span>
<span id="cb4-14"><a href="#cb4-14" aria-hidden="true" tabindex="-1"></a>      <span class="cf">return</span> prop</span>
<span id="cb4-15"><a href="#cb4-15" aria-hidden="true" tabindex="-1"></a>    <span class="cf">end</span></span></code></pre></div>
<h1 id="samples">Samples</h1>
<p>Write a module for a debugger can be tricky, that is why we thought samples could help. We will continue on the <em>car</em> use case. Here the idea is to provide debugger display modules for the following code.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> <span class="cn">C</span> <span class="op">=</span> <span class="op">{}</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> mt <span class="op">=</span> <span class="op">{</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a>    <span class="bu">__index</span> <span class="op">=</span> <span class="kw">function</span><span class="op">(</span>self<span class="op">,</span> key<span class="op">)</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a>      <span class="cf">return</span> self<span class="op">[</span><span class="fu">string.format</span><span class="op">(</span><span class="st">&#39;_%s&#39;</span><span class="op">,</span>key <span class="kw">or</span> <span class="st">&#39;&#39;</span><span class="op">)]</span> <span class="kw">or</span> <span class="st">&#39;Nameless&#39;</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a>    <span class="kw">end</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a>  <span class="op">}</span></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> id <span class="op">=</span> <span class="dv">1</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a>  <span class="kw">function</span> <span class="cn">C</span><span class="op">.</span>newcar<span class="op">(</span>name<span class="op">)</span></span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> car <span class="op">=</span> <span class="op">{</span> <span class="cn">_</span>id <span class="op">=</span> id <span class="op">,</span> <span class="cn">_</span>name <span class="op">=</span> name <span class="op">,</span> <span class="cn">_</span>type <span class="op">=</span> <span class="st">&#39;Car&#39;</span> <span class="op">}</span></span>
<span id="cb5-10"><a href="#cb5-10" aria-hidden="true" tabindex="-1"></a>    id <span class="op">=</span> id <span class="op">+</span> <span class="dv">1</span></span>
<span id="cb5-11"><a href="#cb5-11" aria-hidden="true" tabindex="-1"></a>    <span class="fu">setmetatable</span><span class="op">(</span>car<span class="op">,</span> mt<span class="op">)</span></span>
<span id="cb5-12"><a href="#cb5-12" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> car</span>
<span id="cb5-13"><a href="#cb5-13" aria-hidden="true" tabindex="-1"></a>  <span class="kw">end</span></span>
<span id="cb5-14"><a href="#cb5-14" aria-hidden="true" tabindex="-1"></a>  </span>
<span id="cb5-15"><a href="#cb5-15" aria-hidden="true" tabindex="-1"></a>  <span class="cf">return</span> <span class="cn">C</span></span></code></pre></div>
<center>
<p><small>car.lua</small></p>
</center>
<div class="sourceCode" id="cb6"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a>  <span class="cf">if</span> <span class="fu">os.getenv</span><span class="op">(</span><span class="st">&#39;DEBUG_MODE&#39;</span><span class="op">)</span> <span class="cf">then</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>    <span class="fu">require</span> <span class="st">&#39;carintrospection&#39;</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>  <span class="cf">end</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a>  </span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> car <span class="op">=</span> <span class="fu">require</span> <span class="st">&#39;car&#39;</span></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> c <span class="op">=</span> car<span class="op">.</span>newcar<span class="op">(</span><span class="st">&#39;Chevrolet&#39;</span><span class="op">)</span></span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a>  <span class="fu">print</span> <span class="op">(</span> c<span class="op">.</span>name <span class="op">)</span></span></code></pre></div>
<center>
<p><small>main.lua</small></p>
</center>
<h2 id="metatable_matching_1">Metatable matching</h2>
<p>Some insight about writing a <em>metatable matching</em> display module. This is the <em>faster</em> method as it is call only when adequate.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> introspection <span class="op">=</span> <span class="fu">require</span> <span class="st">&#39;debugger.introspection&#39;</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a>  </span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> function car_inspector<span class="op">(</span>name<span class="op">,</span> value<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> carlabel <span class="op">=</span> <span class="fu">string.format</span><span class="op">(</span><span class="st">&#39;%s (car:%d)&#39;</span><span class="op">,</span> value<span class="op">.</span>name<span class="op">,</span> value<span class="op">.</span>id<span class="op">)</span></span>
<span id="cb7-5"><a href="#cb7-5" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> property <span class="op">=</span> introspection<span class="op">.</span>property<span class="op">(</span>name<span class="op">,</span> <span class="st">&#39;Car&#39;</span><span class="op">,</span> carlabel<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb7-6"><a href="#cb7-6" aria-hidden="true" tabindex="-1"></a>    <span class="co">-- Inspect children</span></span>
<span id="cb7-7"><a href="#cb7-7" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;id&#39;</span><span class="op">,</span> <span class="fu">tostring</span><span class="op">(</span>value<span class="op">.</span>id<span class="op">),</span> property<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;id&#39;</span><span class="op">))</span></span>
<span id="cb7-8"><a href="#cb7-8" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;name&#39;</span><span class="op">,</span> value<span class="op">.</span>name<span class="op">,</span> property<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;name&#39;</span><span class="op">))</span></span>
<span id="cb7-9"><a href="#cb7-9" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> property</span>
<span id="cb7-10"><a href="#cb7-10" aria-hidden="true" tabindex="-1"></a>  <span class="cf">end</span></span>
<span id="cb7-11"><a href="#cb7-11" aria-hidden="true" tabindex="-1"></a>  </span>
<span id="cb7-12"><a href="#cb7-12" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> car <span class="op">=</span> <span class="fu">require</span> <span class="st">&#39;car&#39;</span></span>
<span id="cb7-13"><a href="#cb7-13" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> mt  <span class="op">=</span> <span class="fu">getmetatable</span><span class="op">(</span>car<span class="op">.</span>newcar<span class="op">(</span><span class="st">&#39;Debugger inspector&#39;</span><span class="op">))</span></span>
<span id="cb7-14"><a href="#cb7-14" aria-hidden="true" tabindex="-1"></a>  <span class="cf">if</span> mt <span class="cf">then</span></span>
<span id="cb7-15"><a href="#cb7-15" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>inspectors<span class="op">[</span> mt <span class="op">]</span> <span class="op">=</span> car_inspector</span>
<span id="cb7-16"><a href="#cb7-16" aria-hidden="true" tabindex="-1"></a>  <span class="cf">end</span></span></code></pre></div>
<h2 id="generic_probe_1">Generic probe</h2>
<p>Here is a <em>generic probe</em> display module for given sample. This method is applicable to a wider range of use cases. Use it with caution, as it call for each value displayed by the debugger.</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode lua"><code class="sourceCode lua"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> introspection <span class="op">=</span> <span class="fu">require</span> <span class="st">&#39;debugger.introspection&#39;</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a>  <span class="kw">local</span> function carprobe<span class="op">(</span>name<span class="op">,</span> value<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-5"><a href="#cb8-5" aria-hidden="true" tabindex="-1"></a>    <span class="cf">if</span> value<span class="op">.</span><span class="cn">_</span>type <span class="op">~=</span> <span class="st">&#39;Car&#39;</span> <span class="cf">then</span></span>
<span id="cb8-6"><a href="#cb8-6" aria-hidden="true" tabindex="-1"></a>      <span class="cf">return</span> <span class="kw">nil</span></span>
<span id="cb8-7"><a href="#cb8-7" aria-hidden="true" tabindex="-1"></a>    <span class="cf">end</span></span>
<span id="cb8-8"><a href="#cb8-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-9"><a href="#cb8-9" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> carlabel <span class="op">=</span> <span class="fu">string.format</span><span class="op">(</span><span class="st">&#39;%s (car:%d)&#39;</span><span class="op">,</span> value<span class="op">.</span>name<span class="op">,</span> value<span class="op">.</span>id<span class="op">)</span></span>
<span id="cb8-10"><a href="#cb8-10" aria-hidden="true" tabindex="-1"></a>    <span class="kw">local</span> property <span class="op">=</span> introspection<span class="op">.</span>property<span class="op">(</span>name<span class="op">,</span> <span class="st">&#39;Car&#39;</span><span class="op">,</span> carlabel<span class="op">,</span> parent<span class="op">,</span> fullname<span class="op">)</span></span>
<span id="cb8-11"><a href="#cb8-11" aria-hidden="true" tabindex="-1"></a>    <span class="co">-- Inspect children</span></span>
<span id="cb8-12"><a href="#cb8-12" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;id&#39;</span><span class="op">,</span> <span class="fu">tostring</span><span class="op">(</span>value<span class="op">.</span>id<span class="op">),</span> property<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;id&#39;</span><span class="op">))</span></span>
<span id="cb8-13"><a href="#cb8-13" aria-hidden="true" tabindex="-1"></a>    introspection<span class="op">.</span>inspect<span class="op">(</span><span class="st">&#39;name&#39;</span><span class="op">,</span> value<span class="op">.</span>name<span class="op">,</span> property<span class="op">,</span> introspection<span class="op">.</span>make_fullname<span class="op">(</span>fullname<span class="op">,</span> <span class="st">&#39;name&#39;</span><span class="op">))</span></span>
<span id="cb8-14"><a href="#cb8-14" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> property</span>
<span id="cb8-15"><a href="#cb8-15" aria-hidden="true" tabindex="-1"></a>  <span class="cf">end</span></span>
<span id="cb8-16"><a href="#cb8-16" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-17"><a href="#cb8-17" aria-hidden="true" tabindex="-1"></a>  introspection<span class="op">.</span>add_probe<span class="op">(</span> carprobe <span class="op">)</span></span></code></pre></div>
<h1 id="limitations">Limitations</h1>
<p>As we’ve seen before, the debugger engine is currently unable to use anything else than indexation to retrieve values or set values after inspection phase, so you will be able to use this feature only if your userdata (or table) implements <code>__index</code> metamethod correctly.</p>
<p>For setting data, the debugger makes basically <code>dostring(fullname .. ' = ' .. newvalue)</code> in a sandboxed environment. So, <code>__newindex</code> metamethod must be implemented if you want to modify data.</p>