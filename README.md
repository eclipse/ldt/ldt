> **Warning:** LDT is a usable tool but it is not maintained anymore. 
> More details available at [here](https://gitlab.eclipse.org/eclipse/ldt/ldt/-/issues/1). 

![ldt_logo](https://eclipse.dev/ldt/images/Ldt_logo.png)

**Lua Development Tools** (LDT) is about providing Lua developers with an IDE providing the user experience developers expect from any other tool dedicated to a static programming language.

